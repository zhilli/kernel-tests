# storage/block/RHEL_61200_multipath_device_show_100%_util_even_idle

Storage: I/O stats for paths under multipath device show 100% utilization even while idle

## How to run it

Please refer to the top-level README.md for common dependencies.

### Install dependencies

```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test

```bash
bash ./runtest.sh
```
